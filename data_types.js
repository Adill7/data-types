function backToFront(str, count) {
    if (count >= str.length) {
      return str; 
    } else {
      const front = str.slice(-count); 
      const back = str.slice(0, -count); 
  
      return front + back + str; 
    }
  }